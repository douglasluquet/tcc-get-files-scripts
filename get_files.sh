#!/bin/bash

export base_path=files

# create dirs
mkdir -p $base_path/packet_loss/2017-apr_2018-aug/
mkdir -p $base_path/maximum_rtt/2017-apr_2018-aug/
mkdir -p $base_path/MOS/2017-apr_2018-aug/
mkdir -p $base_path/unpredictability/2017-apr_2018-aug/
mkdir -p $base_path/unreachability/2017-apr_2018-aug/

# packet_loss - 2017-apr to 2018-aug (17 months)
echo "packet_loss - 2017-apr to 2018-aug (17 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2017&month=[04-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2017-apr_2018-aug/packet_loss-2017_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2018&month=[01-08]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2017-apr_2018-aug/packet_loss-2018_#1"

# maximum_rtt - 2017-apr to 2018-aug (17 months)
echo "maximum_rtt - 2017-apr to 2018-aug (17 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2017&month=[04-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2017-apr_2018-aug/maximum_rtt-2017_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2018&month=[01-08]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2017-apr_2018-aug/maximum_rtt-2018_#1"

# MOS - 2017-apr to 2018-aug (17 months)
echo "MOS - 2017-apr to 2018-aug (17 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2017&month=[04-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2017-apr_2018-aug/MOS-2017_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2018&month=[01-08]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2017-apr_2018-aug/MOS-2018_#1"

# unpredictability - 2017-apr to 2018-aug (17 months)
echo "unpredictability - 2017-apr to 2018-aug (17 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2017&month=[04-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2017-apr_2018-aug/unpredictability-2017_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2018&month=[01-08]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2017-apr_2018-aug/unpredictability-2018_#1"

# unreachability - 2017-apr to 2018-aug (17 months)
echo "unreachability - 2017-apr to 2018-aug (17 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2017&month=[04-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2017-apr_2018-aug/unreachability-2017_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2018&month=[01-08]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2017-apr_2018-aug/unreachability-2018_#1"

#####################################################################################################################################################################

# create dirs
mkdir -p $base_path/packet_loss/2016-jan_2018-aug/
mkdir -p $base_path/maximum_rtt/2016-jan_2018-aug/
mkdir -p $base_path/MOS/2016-jan_2018-aug/
mkdir -p $base_path/unpredictability/2016-jan_2018-aug/
mkdir -p $base_path/unreachability/2016-jan_2018-aug/

# packet_loss - 2016-jan to 2018-aug (32 months)
echo "packet_loss - 2016-jan to 2018-aug (32 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2016&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2016-jan_2018-aug/packet_loss-2016_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2017&month=[01-03]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2016-jan_2018-aug/packet_loss-2017_#1"

# maximum_rtt - 2016-jan to 2018-aug (32 months)
echo "maximum_rtt - 2016-jan to 2018-aug (32 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2016&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2016-jan_2018-aug/maximum_rtt-2016_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2017&month=[01-03]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2016-jan_2018-aug/maximum_rtt-2017_#1"

# MOS - 2016-jan to 2018-aug (32 months)
echo "MOS - 2016-jan to 2018-aug (32 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2016&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2016-jan_2018-aug/MOS-2016_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2017&month=[01-03]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2016-jan_2018-aug/MOS-2017_#1"

# unpredictability - 2016-jan to 2018-aug (32 months)
echo "unpredictability - 2016-jan to 2018-aug (32 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2016&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2016-jan_2018-aug/unpredictability-2016_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2017&month=[01-03]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2016-jan_2018-aug/unpredictability-2017_#1"

# unreachability - 2016-jan to 2018-aug (32 months)
echo "unreachability - 2016-jan to 2018-aug (32 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2016&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2016-jan_2018-aug/unreachability-2016_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2017&month=[01-03]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2016-jan_2018-aug/unreachability-2017_#1"

# copy existent files
echo "Coping file from 2017-apr_2018-aug to 2016-jan_2018-aug"
cp $base_path/packet_loss/2017-apr_2018-aug/* $base_path/packet_loss/2016-jan_2018-aug/
cp $base_path/maximum_rtt/2017-apr_2018-aug/* $base_path/maximum_rtt/2016-jan_2018-aug/
cp $base_path/MOS/2017-apr_2018-aug/* $base_path/MOS/2016-jan_2018-aug/
cp $base_path/unpredictability/2017-apr_2018-aug/* $base_path/unpredictability/2016-jan_2018-aug/
cp $base_path/unreachability/2017-apr_2018-aug/* $base_path/unreachability/2016-jan_2018-aug/


#####################################################################################################################################################################

# create dirs
mkdir -p $base_path/packet_loss/2014-jan_2018-aug/
mkdir -p $base_path/maximum_rtt/2014-jan_2018-aug/
mkdir -p $base_path/MOS/2014-jan_2018-aug/
mkdir -p $base_path/unpredictability/2014-jan_2018-aug/
mkdir -p $base_path/unreachability/2014-jan_2018-aug/

# packet_loss - 2014-jan to 2018-aug (45 months)
echo "packet_loss - 2014-jan to 2018-aug (45 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2014-jan_2018-aug/packet_loss-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2014-jan_2018-aug/packet_loss-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=packet_loss&by=by-node&size=100&tick=daily&year=2015&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/packet_loss/2014-jan_2018-aug/packet_loss-2015_#1"


# maximum_rtt - 2014-jan to 2018-aug (45 months)
echo "maximum_rtt - 2014-jan to 2018-aug (45 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2014-jan_2018-aug/maximum_rtt-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=maximum_rtt&by=by-node&size=100&tick=daily&year=2015&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/maximum_rtt/2014-jan_2018-aug/maximum_rtt-2015_#1"


# MOS - 2014-jan to 2018-aug (45 months)
echo "MOS - 2014-jan to 2018-aug (45 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2014-jan_2018-aug/MOS-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=MOS&by=by-node&size=100&tick=daily&year=2015&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/MOS/2014-jan_2018-aug/MOS-2015_#1"


# unpredictability - 2014-jan to 2018-aug (45 months)
echo "unpredictability - 2014-jan to 2018-aug (45 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2014-jan_2018-aug/unpredictability-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unpredictability&by=by-node&size=100&tick=daily&year=2015&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unpredictability/2014-jan_2018-aug/unpredictability-2015_#1"


# unreachability - 2014-jan to 2018-aug (45 months)
echo "unreachability - 2014-jan to 2018-aug (45 months)"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2014&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2014-jan_2018-aug/unreachability-2014_#1"
curl "http://www-wanmon.slac.stanford.edu/cgi-wrap/pingtable.pl?format=tsv&file=unreachability&by=by-node&size=100&tick=daily&year=2015&month=[01-12]&from=WORLD&to=WORLD&ex=none&only=all&ipv=all&dataset=hep&percentage=any&dnode=on" --output "$base_path/unreachability/2014-jan_2018-aug/unreachability-2015_#1"


# copy existent files
echo "Coping file from 2016-jan_2018-aug to 2014-jan_2018-aug"
cp $base_path/packet_loss/2016-jan_2018-aug/* $base_path/packet_loss/2014-jan_2018-aug/
cp $base_path/maximum_rtt/2016-jan_2018-aug/* $base_path/maximum_rtt/2014-jan_2018-aug/
cp $base_path/MOS/2016-jan_2018-aug/* $base_path/MOS/2014-jan_2018-aug/
cp $base_path/unpredictability/2016-jan_2018-aug/* $base_path/unpredictability/2014-jan_2018-aug/
cp $base_path/unreachability/2016-jan_2018-aug/* $base_path/unreachability/2014-jan_2018-aug/

# python transformation
python src/transform_files.py   