import csv, calendar, sys

def month_name(month_number):
    try: 
        month_number = int(month_number)
        return calendar.month_name[month_number][:3]
    except:
        print(sys.exc_info()[0])

def metric_map(metric_name):
    map = { "packet_loss":"PacketLoss", "maximum_rtt":"MaximumRTT", "MOS":"MOS", "unpredictability":"Unpredictability", "unreachability":"Unreachability" }
    return map[metric_name]

def main():
    base_path = "./files"
    
    metrics = ["packet_loss","maximum_rtt","MOS","unpredictability","unreachability"]
    
    #2015-apr_2018-aug
    intervals = ["2017-apr_2018-aug", "2016-jan_2018-aug", "2014-jan_2018-aug"]   
    
    # Maio de 2015 com erro, só vem valr até o dia 30.
    months = [
        ["2018_01",0], ["2018_02",3], ["2018_03",0], ["2018_04",1], ["2018_05",0], ["2018_06",1], ["2018_07",0], ["2018_08",0], 
        ["2017_01",0], ["2017_02",3], ["2017_03",0], ["2017_04",1], ["2017_05",0], ["2017_06",1], ["2017_07",0], ["2017_08",0], ["2017_09",1], ["2017_10",0], ["2017_11",1], ["2017_12",0],
        ["2016_01",0], ["2016_02",2], ["2016_03",0], ["2016_04",1], ["2016_05",0], ["2016_06",1], ["2016_07",0], ["2016_08",0], ["2016_09",1], ["2016_10",0], ["2016_11",1], ["2016_12",0],
        ["2015_01",0], ["2015_02",3], ["2015_03",0], ["2015_04",1], ["2015_05",1], ["2015_06",1], ["2015_07",0], ["2015_08",0], ["2015_09",1], ["2015_10",0], ["2015_11",1], ["2015_12",0],
        ["2014_01",0], ["2014_02",3], ["2014_03",0], ["2014_04",1], ["2014_05",1], ["2014_06",1], ["2014_07",0], ["2014_08",0], ["2014_09",1], ["2014_10",0], ["2014_11",1], ["2014_12",0]]
    
    
    for metric in metrics:
        for interval in intervals:
            for month in months:
            
                try:
                    # ex: /home/douglas/data/tcc/proj/files/maximum_rtt/2017-apr_2018-aug/maximum_rtt-2018_02
                    cr = csv.reader(open("{0}/{1}/{2}/{1}-{3}".format(
                        base_path,
                        metric,
                        interval,
                        month[0]
                    ), "r"), delimiter = '\t')
    
                    output = csv.writer(open("{0}/{1}/{2}/pinger-{1}-100-by-node-{3}-{4}.tsv".format(
                        base_path,
                        metric,
                        interval,
                        month[0].split("_")[0],
                        month[0].split("_")[1]
                    ), "w"), lineterminator="\n", delimiter = '\t')
    
                    postion = 34 - month[1]
                    for row in cr:    
                        for i in range(month[1]):
                            row.insert(postion + i, ".")
                        row.append(metric_map(metric))
                        row.append(month[0].split("_")[0][-2:])
                        row.append(month_name(month[0].split("_")[1]))
                        output.writerow(row)

                except Exception as e:
                    print("Skipping this file: {0}{1} for interval {2}".format(metric, month[0], interval))
                    print(e)

if __name__ == "__main__":
    main()